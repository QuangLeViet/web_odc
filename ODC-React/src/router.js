import React from 'react';
import PageHome from "./pages/home/pages/PageHome";
import PageAdmin from "./pages/admin/pages/PageAdmin";
import PageLogin from "./pages/sign/pages/PageLogin";
import PageUsers from "./pages/admin/pages/User/PageUsers";
import PageWhySna from "./pages/home/pages/PageWhySna";
import MainLayout from "./pages/home/MainLayout";
import PageProcess from "./pages/home/pages/PageProcess";
import PagePortfolio from "./pages/home/pages/PagePortfolio";
import PageDetailPortfolio from "./pages/home/pages/PageDetailPortfolio";
import PageContactUs from "./pages/home/pages/PageContactUs";

export const list_router_pages = [
   {
      path: '/login',
      exact: false,
      main: () => <PageLogin/>
   },
   {
      path: '/',
      exact: false,
      main: () => <MainLayout/>,
      routes: [
         {
            path: '',
            exact: true,
            main: () => <PageHome/>
         },
         {
            path: 'home',
            exact: true,
            main: () => <PageHome/>
         },
         {
            path: 'why-us',
            exact: true,
            main: () => <PageWhySna/>
         },
         {
            path: 'process',
            exact: true,
            main: () => <PageProcess/>
         },
         {
            path: 'portfolio',
            exact: true,
            main: () => <PagePortfolio/>
         },
         {
            path: 'portfolio/:id',
            exact: true,
            main: () => <PageDetailPortfolio/>
         },
         {
            path: 'about-us',
            exact: true,
            main: () => <PageContactUs/>
         },
      ]
   },
   {
      path: '/admin',
      exact: false,
      main: () => <PageAdmin/>,
      routes: [
         {
            path: '/',
            exact: true,
            main: () => <PageUsers/>
         },
         {
            path: '/users',
            exact: false,
            main: () => <PageUsers/>
         },
         {
            path: '/portfolio',
            exact: false,
            main: () => <PagePortfolio/>
         },
      ]
   },
];

import $ from 'jquery';

$(function () {
//sidebar menu js
   $.sidebarMenu($('.sidebar-menu'));

// === toggle-menu js
   $(".toggle-menu").on("click", function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
   });

   $(function () {
      for (var i = window.location, o = $(".sidebar-menu a").filter(function () {
         return this.href === i;
      }).addClass("active").parent().addClass("active"); ;) {
         if (!o.is("li")) break;
         o = o.parent().addClass("in").parent().addClass("active");
      }
   });

   // page loader

   $(window).on('load', function () {

      $('.loader-wrapper').fadeOut(1500);

   })


   $(function () {
      $('[data-toggle="popover"]').popover()
   })


   $(function () {
      $('[data-toggle="tooltip"]').tooltip()
   })

});

$(document).ready(function () {

   $(".navbar a").on("click", function (event) {
      if (this.hash !== "") {
         event.preventDefault();
         var hash = this.hash;

         const top = $(hash).offset().top;
         if (window.innerWidth < 992) {
            $(".navbar-toggler").trigger("click");
         }
         $("html,body")
            .stop()
            .animate(
               {
                  scrollTop: top
               },
               600,
               function () {
                  window.location.hash = hash;
               }
            );
      }
   });
});

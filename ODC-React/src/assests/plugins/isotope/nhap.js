document.addEventListener('DOMContentLoaded', function () {
   var grid = null;
   var docElem = document.documentElement;
   var demo = document.querySelector('.grid-demo');
   var gridElement = demo.querySelector('.grid');
   var filterField = demo.querySelector('.filter-field');
   var searchField = demo.querySelector('.search-field');
   var addItemsElement = demo.querySelector('.add-more-items');
   var characters = 'abcdefghijklmnopqrstuvwxyz';
   var filterOptions = ['red', 'blue', 'green'];
   var dragOrder = [];
   var uuid = 0;
   var filterFieldValue;
   var searchFieldValue;


   function initDemo() {

      initGrid();

      // Reset field values.
      searchField.value = '';
      [filterField].forEach(function (field) {
         field.value = field.querySelectorAll('option')[0].value;
      });

      // Set inital search query, active filter, active sort value and active layout.
      searchFieldValue = searchField.value.toLowerCase();
      filterFieldValue = filterField.value;

      // Search field binding.
      searchField.addEventListener('keyup', function () {
         var newSearch = searchField.value.toLowerCase();
         if (searchFieldValue !== newSearch) {
            searchFieldValue = newSearch;
            filter();
         }
      });

      // Filter, sort and layout bindings.
      filterField.addEventListener('change', filter);

      // Add/remove items bindings.
      addItemsElement.addEventListener('click', addItems);
      gridElement.addEventListener('click', function (e) {
         if (elementMatches(e.target, '.card-remove, .card-remove i')) {
            removeItem(e);
         }
      });

   }

   function initGrid() {

      var dragCounter = 0;

      grid = new Muuri(gridElement, {
         items: generateElements(20),
         layoutDuration: 400,
         layoutEasing: 'ease',
         dragEnabled: true,
         dragSortInterval: 50,
         dragContainer: document.body,
         dragStartPredicate: function (item, event) {
            var isRemoveAction = elementMatches(event.target, '.card-remove, .card-remove i');
            return !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
         },
         dragReleaseDuration: 400,
         dragReleseEasing: 'ease'
      })
         .on('dragStart', function () {
            ++dragCounter;
            docElem.classList.add('dragging');
         })
         .on('dragEnd', function () {
            if (--dragCounter < 1) {
               docElem.classList.remove('dragging');
            }
         })
         .on('move', updateIndices)
         .on('sort', updateIndices);

   }

   function filter() {

      filterFieldValue = filterField.value;
      grid.filter(function (item) {
         var element = item.getElement();
         var isSearchMatch = !searchFieldValue ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
         var isFilterMatch = !filterFieldValue ? true : (element.getAttribute('data-color') || '') === filterFieldValue;
         return isSearchMatch && isFilterMatch;
      });

   }

   function generateElements(amount) {

      var ret = [];

      for (var i = 0, len = amount || 1; i < amount; i++) {

         var id = ++uuid;
         var color = getRandomItem(filterOptions);
         var title = generateRandomWord(2);
         var width = Math.floor(Math.random() * 2) + 1;
         var height = Math.floor(Math.random() * 2) + 1;
         var itemElem = document.createElement('div');
         var itemTemplate = '' +
            '<div class="item h' + height + ' w' + width + ' ' + color + '" data-id="' + id + '" data-color="' + color + '" data-title="' + title + '">' +
            '<div class="item-content">' +
            '<div class="card">' +
            '<div class="card-id">' + id + '</div>' +
            '<div class="card-title">' + title + '</div>' +
            '<div class="card-remove"><i class="material-icons">&#xE5CD;</i></div>' +
            '</div>' +
            '</div>' +
            '</div>';

         itemElem.innerHTML = itemTemplate;
         ret.push(itemElem.firstChild);

      }

      return ret;

   }

   function getRandomItem(collection) {

      return collection[Math.floor(Math.random() * collection.length)];

   }

   function generateRandomWord(length) {

      var ret = '';
      for (var i = 0; i < length; i++) {
         ret += getRandomItem(characters);
      }
      return ret;

   }

   function compareItemTitle(a, b) {

      var aVal = a.getElement().getAttribute('data-title') || '';
      var bVal = b.getElement().getAttribute('data-title') || '';
      return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

   }

   function compareItemColor(a, b) {

      var aVal = a.getElement().getAttribute('data-color') || '';
      var bVal = b.getElement().getAttribute('data-color') || '';
      return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle(a, b);

   }

   function updateIndices() {

      grid.getItems().forEach(function (item, i) {
         item.getElement().setAttribute('data-id', i + 1);
         item.getElement().querySelector('.card-id').innerHTML = i + 1;
      });

   }

   function elementMatches(element, selector) {

      var p = Element.prototype;
      return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

   }

   function elementClosest(element, selector) {

      if (window.Element && !Element.prototype.closest) {
         var isMatch = elementMatches(element, selector);
         while (!isMatch && element && element !== document) {
            element = element.parentNode;
            isMatch = element && element !== document && elementMatches(element, selector);
         }
         return element && element !== document ? element : null;
      }
      else {
         return element.closest(selector);
      }

   }
   initDemo();

});
import React, {Component} from 'react';
import './index.css';

class OurProcess extends Component {

   render() {
      return (
         <React.Fragment>
            <section className="process">
               <div className="bg-process-image"/>
               <div className="bg-process-white"/>
               <div className="position-absolute w-100 top-0 left-0">
                  <div className="container">

                     <p className="custom_title mt-3 mt-md-5">
                        <span className="color_sna">OUR</span> PROCESS
                     </p>

                     {/*item 1*/}
                     <div className="process-item align-items-center animated bounceInLeft d-flex delay-1">
                        <div className="process-item-left">
                           <div className="box_process box_process_1 w-100 h-100"/>
                        </div>
                        <div className="process-item-right d-flex align-items-center">
                           <div className="process_img process_img_1 "/>
                           <div className="process-content">
                              <div className="process-content-header font-RoR mb-1">initial meeting
                              </div>
                              <div className="process-content-body font-RoL"> We will have an initial conference call to
                                 understand what the key to your success is. During the next few meetings we will find
                                 together the best way to achieve your goal.
                              </div>
                           </div>
                        </div>
                     </div>

                     {/*item 2*/}
                     <div className="process-item align-items-center animated bounceInLeft d-flex delay-2">
                        <div className="process-item-left">
                           <div className="box_process box_process_2 w-100 h-100"/>
                        </div>
                        <div className="process-item-right d-flex align-items-center">
                           <div className="process_img process_img_2 "/>
                           <div className="process-content">
                              <div className="process-content-header font-RoR mb-1">alignment & proposal
                              </div>
                              <div className="process-content-body font-RoL">
                                 After key achievement is settled, we will offer a custom proposal <br/>
                                 This is the step for signing main contract and terms which agree on budget and scope
                              </div>
                           </div>
                        </div>
                     </div>

                     {/*item 3*/}
                     <div className="process-item align-items-center animated bounceInLeft d-flex delay-3">
                        <div className="process-item-left">
                           <div className="box_process box_process_3 w-100 h-100"/>
                        </div>
                        <div className="process-item-right d-flex align-items-center">
                           <div className="process_img process_img_3 "/>
                           <div className="process-content">
                              <div className="process-content-header font-RoR mb-1">creating team
                              </div>
                              <div className="process-content-body font-RoL">
                                 We will establish your own team with proper developers, UI/UX designers, QA engineers
                                 and many more who fit your project best.
                              </div>
                           </div>
                        </div>
                     </div>

                     {/*item 4*/}
                     <div className="process-item align-items-center animated bounceInLeft d-flex delay-4">
                        <div className="process-item-left">
                           <div className="box_process box_process_4 w-100 h-100"/>
                        </div>
                        <div className="process-item-right d-flex align-items-center">
                           <div className="process_img process_img_4 "/>
                           <div className="process-content">
                              <div className="process-content-header font-RoR mb-1">kick-off
                              </div>
                              <div className="process-content-body font-RoL">
                                 Your team will understand your goal and project base will be created. <br/>
                                 We use practical tools such as Jira, Slack, Zoom for clear and concise communication.
                              </div>
                           </div>
                        </div>
                     </div>

                     {/*item 5*/}
                     <div className="process-item align-items-center animated bounceInLeft d-flex delay-5">
                        <div className="process-item-left">
                           <div className="box_process box_process_5 w-100 h-100"/>
                        </div>
                        <div className="process-item-right d-flex align-items-center">
                           <div className="process_img process_img_5 "/>
                           <div className="process-content">
                              <div className="process-content-header font-RoR mb-1">project analysis
                              </div>
                              <div className="process-content-body font-RoL">
                                 We will analyze for clear understandings of each other and get deeper knowledge on your
                                 project. This step we will have a road map to minimize loose ends of plan.
                              </div>
                           </div>
                        </div>
                     </div>

                     {/*item 6*/}
                     <div className="process-item align-items-center animated bounceInLeft d-flex delay-6">
                        <div className="process-item-left">
                           <div className="box_process box_process_6 w-100 h-100"/>
                        </div>
                        <div className="process-item-right d-flex align-items-center">
                           <div className="process_img process_img_6 "/>
                           <div className="process-content">
                              <div className="process-content-header font-RoR mb-1">development
                              </div>
                              <div className="process-content-body font-RoL">
                                 Design and development will begin. Projects are managed on Jira. Different development
                                 approaches and meetings will be done on a regular basis depending on the project
                                 characteristics.
                              </div>
                           </div>
                        </div>
                     </div>

                     {/*item 7*/}
                     <div className="process-item align-items-center animated bounceInLeft d-flex delay-7">
                        <div className="process-item-left">
                           <div className="box_process box_process_7 w-100 h-100"/>
                        </div>
                        <div className="process-item-right d-flex align-items-center">
                           <div className="process_img process_img_7 "/>
                           <div className="process-content">
                              <div className="process-content-header font-RoR mb-1">delivery
                              </div>
                              <div className="process-content-body font-RoL">
                                 Software is released and we can continue working on future requirements or maintenance.
                                 If you decide to handover the project information to a new team, we will transfer the
                                 materials and data according to our knowledge transfer process.
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }

}

export default (OurProcess);

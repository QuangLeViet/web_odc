import React, {Component} from 'react';
import './index.css';
import icon1 from './../../../../../../assests/images/whysna/icon1.png';
import icon2 from './../../../../../../assests/images/whysna/icon2.png';
import icon3 from './../../../../../../assests/images/whysna/icon3.png';
class OwnTeam extends Component {

   render() {
      return (
         <React.Fragment>
            <section className="ownTeam" id="own-team">
               <div className="container">
                  <p className="font-RoL custom_title text-center">Your Own <span className="color_sna">Team</span></p>
                  <p className="font-RoR ownTeam_content text-center m-auto">
                     With SNA your gola is already success. While we offer services
                     as many offer offshore company do, SNA is pre-cialzed
                     on dedicated team, security and reliability </p>
                  <div className="row no-gutters bg_ownTeam py-4 position-relative">
                     <div className="col-12 col-lg-4 ownTeam_item text-center p-4 d-none animated">
                        <img src={icon1} alt="dedicated" className="my-5"/>
                        <p className="font-RoL ownTeam_item_title mb-4">Dedicated Team</p>
                        <p className="font-RoR ownTeam_item_content">
                           A dedicated team is provided for your project and available to scale up and down within your
                           need. We understand communication is the key to success. All our team members are fluent in
                           verbal and written English.
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 ownTeam_item text-center p-4 d-none animated delay-1">
                        <img src={icon2} alt="dedicated" className="my-5"/>
                        <p className="font-RoL ownTeam_item_title mb-4">Scale Up/Down</p>
                        <p className="font-RoR ownTeam_item_content">
                           Get more developers to speed up the development process or scale down during the maintenance
                           cycle.
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 ownTeam_item text-center p-4 d-none animated delay-2">
                        <img src={icon3} alt="dedicated" className="my-5"/>
                        <p className="font-RoL ownTeam_item_title mb-4">English</p>
                        <p className="font-RoR ownTeam_item_content">
                           All our employees are fluent in verbal and written English, enables precise understanding
                           of your request.
                        </p>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default (OwnTeam);

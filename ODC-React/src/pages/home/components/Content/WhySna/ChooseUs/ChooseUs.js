import React, {Component} from 'react';
import './index.css';
import img_lady from './../../../../../../assests/images/whysna/img1.png';

class ChooseUs extends Component {

   render() {
      return (
         <React.Fragment>
            <section className="chooseUs">
               <div className="container py-3 py-md-5">
                  <p className="font-RoL custom_title">
                     WHY
                     <span className="mx-2 color_sna">CHOOSE</span>
                     US?
                  </p>
                  <p className="font-RoR custom_content m-0">
                     Many companies face multiple issues on recruitment and management while developing a software.
                  </p>
                  <p className="font-RoR custom_content m-0">
                     We offer "One-Stop Package" for your software from recruitment to maintenance.
                  </p>
                  <p className="font-RoR custom_content m-0">
                     Release your valuable resources from development and focus on what matters most.
                  </p>
                  <div className="row no-gutters my-4">
                     <div className="col-12 col-lg-6 d-flex align-items-center px-2">
                        <img src={img_lady} className="w-100" alt="lady"/>
                     </div>
                     <div className="col-12 col-lg-6 d-flex flex-wrap mt-2 mt-lg-0">
                        <div className="chooseUs-item py-1 pl-3 mb-2 w-100 animated bounceInRight delay-4">
                           <p className="font-RoM item-title">Your Own Team</p>
                           <p className="font-RoR item-content">
                             Get to choose the right people to work with. Scale up or down easily within business demands.
                           </p>
                        </div>
                        <div className="chooseUs-item py-1 pl-3 mb-2 w-100 animated bounceInRight delay-5">
                           <p className="font-RoM item-title">Security</p>
                           <p className="font-RoR item-content">
                              Your data is considered very seriously. Per employee NDA along with physical
                              security ( security guard, CCTV, UPS), your project is protected.
                           </p>
                        </div>
                        <div className="chooseUs-item py-1 pl-3 mb-2 w-100 animated bounceInRight delay-6">
                           <p className="font-RoM item-title">Reliability</p>
                           <p className="font-RoR item-content">
                             With our expertise in the industry, your success is ensured. <br/>
                             You have full control in time-to-market.
                           </p>
                        </div>
                        {/*<div className="chooseUs-item d-none py-1 pl-3 w-100 animated delay-3">*/}
                        {/*   <p className="font-RoM item-title">24/7 Support</p>*/}
                        {/*   <p className="font-RoR item-content">*/}
                        {/*      Our highly skilled tech support specialists are ready to help you any time of the day (or night)*/}
                        {/*      We are always within an easy reach.*/}
                        {/*   </p>*/}
                        {/*</div>*/}
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default (ChooseUs);


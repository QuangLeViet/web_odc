import React, {Component} from 'react';
import ChooseUs from "./ChooseUs/ChooseUs";
import OwnTeam from "./OwnTeam/OwnTeam";
import Security from "./Security/Security";
import RopeCurtain from "../Home/RopeCurtain/RopeCurtain";
import Reliability from "./Reliability/Reliability";
import $ from "jquery";
import {withRouter} from 'react-router-dom';


class ContentWhySna extends Component {

   componentDidMount() {
      const view_height = $(window)[0].innerHeight;
      $(window).on("scroll", function () {
         if ($(this).scrollTop() > view_height * 0.5) {
            $('.ownTeam_item.animated').removeClass('d-none').addClass('rollInLeft');
         }
         if ($(this).scrollTop() > view_height * 1) {
            $('.security_item.animated').removeClass('d-none').addClass('bounceIn');
         }
         if ($(this).scrollTop() > view_height * 2) {
            $('.reliability_item.animated').removeClass('d-none').addClass('rollInRight');
         }
      });
      const tag_ele = this.props.location.hash;
      if(tag_ele) {
         setTimeout(function () {
            $('html, body').animate({
               scrollTop: $(tag_ele).offset().top
            }, 500);
         },2000);
      }
   }

   render() {
      return (
         <React.Fragment>

            <ChooseUs/>

            <OwnTeam/>

            <Security/>

            <RopeCurtain/>

            <Reliability/>

         </React.Fragment>
      )
   }
}

export default withRouter(ContentWhySna);

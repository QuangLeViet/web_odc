import React, {Component} from 'react';
import icon1 from "../../../../../../assests/images/whysna/icon4.png";
import icon2 from "../../../../../../assests/images/whysna/icon5.png";
import icon3 from "../../../../../../assests/images/whysna/icon6.png";
import './index.css';

class Security extends Component {

   render() {
      return (
         <React.Fragment>
            <section className="security" id="security">
               <div className="container">
                  <p className="font-RoL custom_title security_title text-white text-center">
                     <span className="color_sna">Security </span>First Policy</p>
                  <p className="font-RoR security_content text-center text-white m-auto">
                     With the Security-First policy, we consider your data and information vary seriously.
                     All project members are required to sign NDA before any project starts. We are well prepared
                     on both logical and physical security to protect your intellectual properties.
                  </p>
                  <div className="row no-gutters align-items-center" style={{minHeight:'500px'}}>
                     <div className="col-12 col-lg-4 security_item d-none  text-center p-4 animated">
                        <img src={icon1} alt="dedicated" className="my-5"/>
                        <p className="font-RoL security_item_title color_sna mb-4">Per Employee NDA</p>
                        <p className="font-RoR security_item_content text-white">
                           To protect your intellectual properties, all our employees are signed on NDA before
                           involving your project.</p>
                     </div>
                     <div className="col-12 col-lg-4 security_item d-none  text-center p-4 animated delay-1">
                        <img src={icon2} alt="dedicated" className="my-5"/>
                        <p className="font-RoL security_item_title color_sna mb-4">Data Security</p>
                        <p className="font-RoR security_item_content text-white">
                           Any data transfer of your project is only allowed in certain circumstances and all
                           data is backed up on a daily basic.
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 security_item d-none  text-center p-4 animated delay-2">
                        <img src={icon3} alt="dedicated" className="my-5"/>
                        <p className="font-RoL security_item_title color_sna mb-4">Physical Security</p>
                        <p className="font-RoR security_item_content text-white">
                           Physical security plays an essential part of security. With 24/7 security guard,
                           CCTV, UPS your project is protected.
                        </p>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default (Security);

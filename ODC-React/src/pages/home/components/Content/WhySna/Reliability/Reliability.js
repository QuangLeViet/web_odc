import React, {Component} from 'react';
import './index.css';
import icon1 from './../../../../../../assests/images/whysna/icon1.png';
import icon2 from './../../../../../../assests/images/whysna/icon8.png';
import icon3 from './../../../../../../assests/images/whysna/icon9.png';

class Reliability extends Component {

   render() {
      return (
         <React.Fragment>
            <section className="reliability" id="reliability">
               <div className="container">
                  <p className="font-RoL custom_title text-center">Reliability
                  </p>
                  <p className="font-RoR reliability_content text-center m-auto">
                     Each our managers are experienced +10 years and embodied with the software development process.
                     Experienced in numerous areas including mobile, finance, retails, and smart factory, we suggest
                     best way to achieve your goal.
                  </p>
                  <div className="row no-gutters bg_reliability py-4 position-relative align-items-center">
                     <div className="col-12 col-lg-4 reliability_item text-center p-3 animated d-none">
                        <img src={icon1} alt="dedicated" className="my-5"/>
                        <p className="font-RoL reliability_item_title mb-4">+10 Yr. Experience</p>
                        <p className="font-RoR reliability_item_content">
                           To protect your intellectual properties, all our employees are signed on NDA before involving
                           your project.
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 reliability_item text-center p-3 animated delay-1 d-none">
                        <img src={icon2} alt="dedicated" className="my-5"/>
                        <p className="font-RoL reliability_item_title mb-4">Strong Fundamental</p>
                        <p className="font-RoR reliability_item_content">
                           SNA is business for 6 years and have experience in various projects including
                           mobile, web, video analytics.
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 reliability_item text-center p-3 animated delay-2 d-none">
                        <img src={icon3} alt="dedicated" className="my-5"/>
                        <p className="font-RoL reliability_item_title mb-4">Partnership</p>
                        <p className="font-RoR reliability_item_content">
                           We have partnership with IBM, Oracle, Microsoft, Amazon for years and
                           have a good relationship in between.
                        </p>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default (Reliability);

import React, {Component} from 'react';
import video from '../../../../../../assests/videos/hero.mp4';
import './../../Home/BGVideo/BGVideo.css';

class BGVideo extends Component {

   componentDidMount() {
   }

   render() {
      const src_IMG = 'https://static1.cbrimages.com/wordpress/wp-content/uploads/2019/10/Demon-Slayer-Similar-Anime-featured-image.jpg';
      const src_VIDEO = video;

      return (
         <React.Fragment>
            {/* Panel */}
            <section className="video">
               <div className="position-relative container-video">
                  <video poster={src_IMG} autoPlay loop muted>
                     <source src={src_VIDEO} type="video/mp4"/>
                  </video>
                  <img src="https://www.kohactive.com/assets/images/hero-still-1699c068.jpg?1577982848"
                       className="w-100" alt="behind"/>
                  <div className={"content-video position-absolute w-100 text-white"}>
                     <div className="container">
                        <p className="font-RoM custom_width_title font-weight-bold m-0">
                           TRANSFER YOUR IDEA INTO REALITY</p>
                        <div className="font-RoR pt-3 custom_width_content">
                           <p>Welcome to your software development team!</p>
                           <p>We build apps, webs and software with a passion</p>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default BGVideo;

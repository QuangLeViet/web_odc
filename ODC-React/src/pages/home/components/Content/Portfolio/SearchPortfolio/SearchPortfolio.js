import React, {Component} from 'react';
import {Select} from 'antd';
import {Input} from 'antd';

import './index.css';

const {Option} = Select;
const {Search} = Input;

class SearchPortfolio extends Component {

   state = {};

   render() {
      return (
         <React.Fragment>
            <section className="searchPortfolio py-3 py-md-5" style={{backgroundColor:'#f9f9f9'}}>
               <div className="container">
                  <div className="row no-gutters">
                     <div className="col-12 col-md-4 px-2">
                        <Select  className="w-100 "
                                 placeholder="Industries"
                                 onChange={this.handleChange}>
                           <Option value="jack">Jack</Option>
                           <Option value="lucy">Lucy</Option>
                        </Select>
                     </div>
                     <div className="col-12 col-md-4 px-2">
                        <Select className="w-100"
                                placeholder="Services"
                                onChange={this.handleChange}>
                           <Option value="jack">Jack</Option>
                           <Option value="lucy">Lucy</Option>
                        </Select>
                     </div>
                     <div className="col-12 col-md-4 px-2">
                        <Search
                           placeholder="input search text"
                           onSearch={value => console.log(value)}
                           className="w-100"
                        />
                     </div>
                  </div>
               </div>
            </section>


         </React.Fragment>
      )
   }

   handleChange = (value) => {
      console.log(`selected ${value}`);
   }

}

export default SearchPortfolio;
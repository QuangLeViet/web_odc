import React, {Component} from 'react';
import './../../Home/Portfolio/index.css';
import $ from 'jquery';
import Shuffle from 'shufflejs/src/shuffle';
import SearchPortfolio from "../SearchPortfolio/SearchPortfolio";
import callApi from "../../../../../../axios";
import {withRouter} from 'react-router-dom';

class Portfolio extends Component {
   shuffle = null;
   state = {
      shuffle: null,
      data: [],
      limit: 6
   };

   componentDidMount() {
      this.GetImage();
   }

   render() {
      return (
         <React.Fragment>
            <section className="portfolio-container">
               <div className="text-center py-4">
                         <span className="custom_title font-RoL">
                         <span className="color_sna">OUR</span> PORTFOLIO</span>
               </div>
               {/*choose group*/}
               <div className="d-flex flex-wrap justify-content-center controls m-auto text-dark pb-3 pb-md-5">
                  <span className="cursor-pointer py-0 px-3 active" data-group="all"
                        onClick={this.onChangeObject}>All</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="smartfactory"
                        onClick={this.onChangeObject}> Smart Factory</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="business"
                        onClick={this.onChangeObject}>Business</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="web"
                        onClick={this.onChangeObject}>Web</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="video"
                        onClick={this.onChangeObject}>Video</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="mobile"
                        onClick={this.onChangeObject}>Mobile</span>
               </div>

               {/*<SearchPortfolio/>*/}

               {/*portfolio item*/}
               <div className="row my-shuffle-container2">
                  {this.state.data.map((item, ind) => (
                     ind < this.state.limit ?
                        <figure key={ind} className={ind % 3 === 0
                           ? "col-6@xs col-6@sm col-6@md picture-item"
                           : "col-3@xs col-6@sm col-3@md picture-item"}
                                data-groups={item.item.tags}
                                data-title={item.item.project_name}>
                           <div className="picture-item__inner"
                                onClick={() => this.HandleClickShowDetail(item.item.iddata)}>
                              <img src={item.img_data}
                                   alt={item.item.project_name}/>
                              <div className="folio-title">
                              <span>
                                 <h2 className="white-color">{item.item.project_name}</h2>
                              </span>
                                 <p className="white-color">{item.item.description_full}</p>
                                 <hr/>
                              </div>
                           </div>
                        </figure> :
                        null
                  ))}
                  <div className="col-1@sm col-1@xs my-sizer-element"/>
               </div>

               {/*load more*/}
               <div className="text-center my-4">
                  <button className="bg-transparent color_sna btn_loadMore font-RoR"
                          disabled={this.state.limit >= this.state.data.length}
                          onClick={this.HandleLoadMore}>LOAD MORE <i className="fa fa-refresh ml-2"/>
                  </button>
               </div>

            </section>
         </React.Fragment>
      )
   }

   GetImage = async () => {
      const self = this;
      // GET ALL LIST PORTFOLIO
      callApi('get', `list-portfolio`).then(async global_data => {
         let ImageDetailArray = [];
         for (let i in global_data.data) {
            global_data.data[i].tags =
               '["' + global_data.data[i].tags.toLowerCase().split(', ').join('","') + '"]';
            // GET EACH IMAGE IN
            await callApi('get', `portfolio-first-image/${global_data.data[i].iddata}`).then(info_image => {
               let bufferBase64 = {};
               let data = info_image.data[0].data.data;
               let buffer = new Buffer(data);
               bufferBase64 = buffer.toString('base64');
               bufferBase64 = "data:image/png;base64," + bufferBase64;
               ImageDetailArray.push({item: global_data.data[i], img_data: bufferBase64});
            })
         }
         this.setState({
            data: ImageDetailArray,
         }, function () {
            self.SetShuffle();
         });
      }, error => {
         console.log(error);
      })
   };


   onChangeObject = (event) => {
      event.persist();
      const btn = event.target;
      $('.controls span').removeClass('active');
      btn.classList.add('active');
      const btnGroup = btn.getAttribute('data-group');
      if (btn.getAttribute('data-group').includes('all')) {
         this.shuffle.filter(Shuffle.ALL_ITEMS);
         return;
      }

      // filter follow groups
      this.shuffle.filter(btnGroup);

      // filter follow title
      // this.shuffle.filter((ele, shuff) => {
      //    if(ele.dataset.title.includes('ac')){
      //       return true;
      //    }
      // });
   };

   HandleLoadMore = () => {
      const self = this;
      const tags = $('.controls span');
      this.setState({
         limit: this.state.limit + 6
      }, function () {
         tags.removeClass('active');
         tags[0].className += ' active';
         self.shuffle.filter(Shuffle.ALL_ITEMS);
         self.SetShuffle();
      })
   };

   SetShuffle = () => {
      $('.picture-item').map((ind, item) => {
         if (item.classList.contains('col-6@md')) {
            item.style.height = item.offsetWidth * 0.61 + 'px';
         } else {
            item.style.height = item.offsetWidth * 1.01 + 'px';
         }
      });
      this.shuffle = new Shuffle(document.getElementsByClassName('my-shuffle-container2')[0], {
         itemSelector: '.picture-item',
         sizer: '.my-sizer-element'
      });
   };

   HandleClickShowDetail = (id) => {
      $(window).scrollTop(0);
      $('.nav-item').removeClass('active');
      $('.portfolio_1').addClass('active');
      this.props.history.push('portfolio/' + id);
   };
}

export default withRouter(Portfolio);

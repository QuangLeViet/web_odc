import React, {Component} from 'react';
import './index.css';
import $ from 'jquery';
import BigImg from './../../../../../../assests/images/process/bg-process.png';
import {withRouter} from 'react-router-dom';
import callApi from "../../../../../../axios";

class OurPortfolio extends Component {

   state = {
      data: {},
      dt_small_images: []
   };

   componentDidMount() {
      // offsetWidth
      callApi('get', `portfolio/${this.props.match.params.id}`).then(async res => {
         this.setState({
            data: res.data[0]
         });
         document.getElementById('parse_html').innerHTML = res.data[0].description_detail;
         let ImageDetailArray = [];
         const data = res.data[0].output.split(',');
         for (let i in data) {
            await callApi('get', `portfolio-image-detail/${data[i]}`).then(info_image => {
               let bufferBase64 = {};
               let data_temp = info_image.data[0].data.data;
               let buffer = new Buffer(data_temp);
               bufferBase64 = buffer.toString('base64');
               bufferBase64 = "data:image/png;base64," + bufferBase64;
               ImageDetailArray.push({img_data: bufferBase64});
            })
         }
         this.setState({dt_small_images: ImageDetailArray})
         setTimeout(function () {
            $('.container-images img').map((ind, ele) => {
               return ele.style.height = ele.offsetWidth + 'px';
            });
         }, 500);
      }, error => {
         console.log(error);
      })

   }

   render() {
      return (
         <React.Fragment>
            <section className="our-portfolio">
               <div className="container pt-3 pt-md-5">
                  <p className="custom_title">
                     <span className="color_sna">OUR</span> PORTFOLIO
                  </p>
                  <div className="w-100">
                     {/*{this.state.dt_small_images.map((dt_image,ind) => (*/}
                     {/*   <img key={ind} src={dt_image.img_data} className="w-50 p-2 pl-md-3 pt-md-3" alt="1"/>*/}
                     {/*))}*/}
                     <img src={this.state.dt_small_images[0] ? this.state.dt_small_images[0].img_data : null}
                          alt="Big" className="w-100 bigImg-portfolio shadow"/>
                     <div className="container-portfolio position-relative">

                        <div className="description-portfolio text-white bg_color_sna py-5 px-4 p-md-4 py-lg-5 px-lg-4">
                           <p className="text-white custom_title font-RoR"> Description Portfolio</p>
                           <table className="font-RoL custom_content">
                              {this.state.data.applied_tech ? (
                                 <tbody>
                                 <tr>
                                    <td>Client:</td>
                                    <td>{this.state.data.client_name}</td>
                                 </tr>
                                 {/*<tr>*/}
                                 {/*   <td>Location:</td>*/}
                                 {/*   <td>{this.state.data.}</td>*/}
                                 {/*</tr>*/}
                                 <tr>
                                    <td>Year Completed:</td>
                                    <td>{this.state.data.end_dttm}</td>
                                 </tr>
                                 {/*<tr>*/}
                                 {/*   <td>Value:</td>*/}
                                 {/*   <td>{this.state.data.}</td>*/}
                                 {/*</tr>*/}
                                 <tr>
                                    <td>Application:</td>
                                    <td>{this.state.data.application_type}</td>
                                 </tr>
                                 <tr>
                                    <td>Technologies:</td>
                                    <td>{this.state.data.applied_tech.split(',').join(', ')}</td>
                                 </tr>
                                 <tr>
                                    <td>Related Tags :</td>
                                    <td>{this.state.data.tags}</td>
                                 </tr>
                                 </tbody>
                              ) : null}
                           </table>
                        </div>
                     </div>
                  </div>

                  <div className="content-portfolio row no-gutters my-3 my-md-5">
                     <div className="col-12 col-md-7">
                        <h3 className="font-RoR color_sna">{this.state.data.project_name}</h3>
                        <div className="my-3 my-md-5 font-RoL custom_content">
                           {this.state.data.description_full}
                        </div>
                        <div id="parse_html" className="parse_html">
                        </div>

                     </div>

                     <div className="col-12 col-md-5 position-relative container-portfolio_img">
                        <div className="container-images">
                           {this.state.dt_small_images.map((dt_image, ind) => (
                              <img key={ind} src={dt_image.img_data} className="w-50 p-2 pl-md-3 pt-md-3" alt="1"/>
                           ))}
                        </div>

                     </div>

                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default withRouter(OurPortfolio);

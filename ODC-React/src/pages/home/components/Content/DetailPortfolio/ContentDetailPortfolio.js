import React, {Component} from 'react';
import OurPortfolio from "./OurPortfolio/OurPortfolio";
import RelatedPortfolio from "./RelatedPortfolio/RelatedPortfolio";

class ContentDetailPortfolio extends Component {

   render() {
      return (
         <React.Fragment>

            <OurPortfolio/>

            <RelatedPortfolio/>

         </React.Fragment>
      )
   }
}

export default (ContentDetailPortfolio);

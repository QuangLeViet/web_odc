import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import $ from "jquery";
import './index.css';
import callApi from "../../../../../../axios";
import {Link} from "react-router-dom";
import Loading from "../../../Loading/Loading";

class RelatedPortfolio extends Component {

   state = {
      data: [],
      loading: false
   };

   componentDidMount() {
      this.GetImage();
   }

   GetImage = async () => {
      this.setState({loading: false})
      // GET ALL LIST PORTFOLIO
      callApi('get', `list-portfolio`).then(async global_data => {
         let ImageDetailArray = [];
         const dt = global_data.data;
         for (let i in dt) {
            dt[i].tags =
               '["' + dt[i].tags.toLowerCase().split(', ').join('","') + '"]';
            // GET EACH IMAGE IN
            await callApi('get', `portfolio-first-image/${dt[i].iddata}`).then(info_image => {
               let bufferBase64 = {};
               let data = info_image.data[0].data.data;
               let buffer = new Buffer(data);
               bufferBase64 = buffer.toString('base64');
               bufferBase64 = "data:image/png;base64," + bufferBase64;
               ImageDetailArray.push({item: dt[i], img_data: bufferBase64});
            })
         }
         this.setState({
            data: ImageDetailArray,
         }, function () {
            $(".portfolio-slider .sliders").slick({
               slidesToShow: 3,
               slidesToScroll: 1,
               prevArrow: `<i class="position-absolute fa fa-caret-left"/>`,
               nextArrow: `<i class="position-absolute fa fa-caret-right"/>`,
               dots: false,
               arrows: true,
               cssEase: "linear",
               responsive: [
                  {
                     breakpoint: 1200,
                     settings: {
                        slidesToShow: 3
                     }
                  },
                  {
                     breakpoint: 768,
                     settings: {
                        slidesToShow: 2
                     }
                  },
                  {
                     breakpoint: 576,
                     settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                     }
                  }
               ]
            });

         });
      }, error => {
         console.log(error);
      })
   };


   render() {
      return (
         <React.Fragment>
            <Loading loading={this.state.loading}/>
            <section className="portfolio-slider w-100 mb-3 mb-md-5">
               <div className="container py-3 py-md-5">
                  <p className="custom_title">
                     <span className="color_sna">RELATED</span> PORTFOLIO
                  </p>
                  <div className="sliders position-relative d-flex ">
                     {this.state.data.map((res, ind) => (
                        ind < 6 ? (
                           <div key={ind} className="p-3">
                              <img src={res.img_data} alt="1" className="w-100"/>
                              <p className="sliders-title my-3 font-RoR">{res.item.project_name}</p>
                              <p className="custom_content font-RoL">{res.item.description_full}</p>
                              <Link className="btn-readMore mt-3 bg_color_sna text-white font-RoR "
                                    to={'/portfolio/' + res.item.iddata}
                                    onClick={() => this.HandleShowDetailPort(res.item.iddata)}>
                                 READ MORE
                              </Link>
                           </div>
                        ) : null
                     ))}
                  </div>
               </div>
            </section>

         </React.Fragment>
      )
   }

   HandleShowDetailPort = (id) => {
      this.setState({loading: true})
      this.props.history.push(id);
      setTimeout(function () {

         $(window).scrollTop(0);
         window.location.reload();
      }, 100)
   }
}

export default withRouter(RelatedPortfolio);

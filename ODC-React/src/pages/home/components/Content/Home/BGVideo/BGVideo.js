import React, {Component} from 'react';
import video from '../../../../../../assests/videos/hero.mp4';
import src_IMG from '../../../../../../assests/images/bg_video.png';
import './BGVideo.css';
import $ from 'jquery';

class BGVideo extends Component {

   componentDidMount() {
      $(window).on('load', function () {
         if (this.innerWidth > 767) {
            document.getElementById('bg_video').play();
         }

      })

   }

   render() {
      const src_VIDEO = video;

      return (
         <React.Fragment>
            {/* Panel */}
            <section className="video">
               <div className="position-relative container-video">
                  <video poster={src_IMG} loop muted id="bg_video">
                     <source src={src_VIDEO} type="video/mp4"/>
                  </video>
                  <img src={src_IMG}
                       className="w-100" alt="behind"/>
                  <div className={"content-video position-absolute w-100 text-white"}>
                     <div className="container">
                        {/*<p className="font-RoM new_title"><span className="badge badge-primary">New</span> Lorem ipsum is simple dummy ?</p>*/}
                        <p className="font-RoM custom_width_title font-weight-bold m-0">
                           Your IDEA, We REALIZE</p>
                        <div className="font-RoR pt-3 custom_width_content">
                           SNA Offshore Development Center offers “One-Stop Package” for your software from the
                           recruitment to the maintenance. Focus on what matters most and let us take care of rest of
                           things.
                        </div>
                        {/*<button*/}
                        {/*   className="font-RoM btn bg_color_sna text-white mt-2 mt-md-4 mt-lg-5 py-1*/}
                        {/*   px-3 py-md-2 px-md-4 py-lg-3 px-lg-5">*/}
                        {/*   START A PROJECT</button>*/}
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default BGVideo;

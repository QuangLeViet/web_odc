import React, {Component} from 'react';
import BGVideo from "./BGVideo/BGVideo";
import HomePortfolio from "./Portfolio/HomePortfolio";
import RopeCurtain from "./RopeCurtain/RopeCurtain";
import Offshore from "./Offshore/Offshore";
import HowWork from "./HowWork/HowWork";
import Sliders from "./Sliders/Sliders";

class ContentHome extends Component {

   render() {
      return (
         <React.Fragment>

            <BGVideo/>

            <Offshore/>

            <HowWork/>

            <RopeCurtain/>

            <HomePortfolio/>

            <Sliders/>

         </React.Fragment>
      )
   }
}

export default ContentHome;

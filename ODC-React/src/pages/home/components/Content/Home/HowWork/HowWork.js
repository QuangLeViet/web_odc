import React, {Component} from 'react';
import './index.css';
import $ from 'jquery';
import arrow from '../../../../../../assests/images/home/howwork/arrow.png';
import {Link} from 'react-router-dom';

class HowWork extends Component {

   state = {
      width: '0px'
   };

   componentDidMount() {
      const widthCircle = $('.container-circle')[0];
      const width_arrow = (widthCircle.parentElement.offsetWidth - widthCircle.offsetWidth) + 8;
      this.setState({
         width: width_arrow + 'px'
      });
      $(window).on("scroll", function () {
         if (600 < $(this).scrollTop() && $(this).scrollTop() < 1000)
            $('.container-circle.animated').addClass('fadeInLeft');
      });
   }

   render() {
      return (
         <React.Fragment>
            <section style={{backgroundColor: '#f7f7f7'}} className="how_work">
               <div className="container py-4">
                  <div className="row py-3 py-md-5 w-100 m-0">
                     <div className="col-12 col-sm-6 col-lg-3 py-2">
                        <div className="container-circle position-relative text-center animated delay-1">
                           <div className="img_circle"/>
                           <p className="mt-2 mb-0 font-RoL">Initial <br/> Meeting</p>
                           <div className="arrow position-absolute">
                              <img src={arrow} alt="arrow" width={this.state.width}/>
                           </div>
                        </div>
                     </div>
                     <div className="col-12 col-sm-6 col-lg-3 py-2">
                        <div className="container-circle position-relative text-center animated delay-2">
                           <div className="img_circle"/>
                           <p className="mt-2 mb-0 font-RoL">Proposal <br/> & Kick Off</p>
                           <div className="arrow position-absolute">
                              <img src={arrow} alt="arrow" width={this.state.width}/>
                           </div>
                        </div>
                     </div>
                     <div className="col-12 col-sm-6 col-lg-3 py-2">
                        <div className="container-circle position-relative text-center animated delay-3">
                           <div className="img_circle"/>
                           <p className="mt-4 mb-0 font-RoL">Development</p>
                           <div className="arrow position-absolute">
                              <img src={arrow} alt="arrow" width={this.state.width}/>
                           </div>
                        </div>
                     </div>
                     <div className="col-12 col-sm-6 col-lg-3 py-2">
                        <div className="container-circle position-relative text-center animated delay-4">
                           <div className="img_circle"/>
                           <p className="mt-4 mb-0 font-RoL">Delivery</p>
                        </div>
                     </div>
                  </div>
                  <div className="w-100 text-center py-3 py-md-5">
                     <p className="content_how_work font-RoR">
                        We will analyze the best way to suit your requirement and achieve your through
                        the already - proven development process.
                        <br/>
                        <Link to={'/process'} onClick={this.HandleClickGoToProCess}
                              className="font-RoM btn btn_contactUs text-uppercase text-white shadow mt-4">
                           Learn More</Link>
                     </p>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
   HandleClickGoToProCess = (id) => {
      $(window).scrollTop(0);
      $('.nav-item').removeClass('active');
      $('.process_1').addClass('active');
   };
}

export default HowWork;

import React, {Component} from 'react';
import $ from "jquery";
import img1 from './../../../../../../assests/images/home/client/client_01.png';
import img2 from './../../../../../../assests/images/home/client/client_02.png';
import img3 from './../../../../../../assests/images/home/client/client_03.png';
import img4 from './../../../../../../assests/images/home/client/client_04.png';
import img5 from './../../../../../../assests/images/home/client/client_05.png';
import img6 from './../../../../../../assests/images/home/client/client_06.png';
import img7 from './../../../../../../assests/images/home/client/client_07.png';
import img8 from './../../../../../../assests/images/home/client/client_08.png';

class Sliders extends Component {

   componentDidMount() {
      $(".autoplay").slick({
         slidesToShow: 5,
         slidesToScroll: 4,
         rows: 1,
         dots: false,
         arrows: false,
         infinite: true,
         autoplay: true,
         autoplaySpeed: 0,
         speed: 15000,
         cssEase: "linear",
         responsive: [
            {
               breakpoint: 1200,
               settings: {
                  slidesToShow: 3
               }
            },
            {
               breakpoint: 768,
               settings: {
                  slidesToShow: 2
               }
            },
            {
               breakpoint: 576,
               settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
               }
            }
         ]
      });
   }

   render() {
      return (
         <React.Fragment>
            <section id="content5">
               <div className="container row m-auto py-3 py-md-5">
                  <p className="custom_title font-RoR text-center w-100">
                     <span className="color_sna">OUR</span> Client's
                  </p>
                  <p className="custom_content font-RoL text-center w-100" style={{color: '#777777'}}>
                     Some of the amazing clients we’ve worked with
                  </p>
                  <div className=" w-100 px-3 pt-4 pb-5 bg-white">
                     <div className="autoplay">
                        <div className="p-4">
                           <img alt="panel" className="rounde m-auto0"
                                src={img1}/>
                        </div>
                        <div className="p-4">
                           <img alt="panel" className="rounded m-auto"
                                src={img2}/>
                        </div>
                        <div className="p-4">
                           <img alt="panel" className="rounded m-auto"
                                src={img3}/>
                        </div>
                        <div className="p-4">
                           <img alt="panel" className="rounded m-auto"
                                src={img4}/>
                        </div>
                        <div className="p-4">
                           <img alt="panel" className="rounded m-auto"
                                src={img5}/>
                        </div>
                        <div className="p-4">
                           <img alt="panel" className="rounded m-auto"
                                src={img6}/>
                        </div>
                        <div className="p-4">
                           <img alt="panel" className="rounded m-auto"
                                src={img7}/>
                        </div>
                        <div className="p-4">
                           <img alt="panel" className="rounded m-auto"
                                src={img8}/>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default Sliders;

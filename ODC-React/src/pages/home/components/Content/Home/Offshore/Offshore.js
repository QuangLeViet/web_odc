import React, {Component} from 'react';
import img from '../../../../../../assests/images/home/offshore/offshore1.png';
import img1 from '../../../../../../assests/images/home/offshore/offshore2.png';
import img2 from '../../../../../../assests/images/home/offshore/offshore3.png';
import './index.css';
import $ from "jquery";
import {withRouter} from 'react-router-dom';

class Offshore extends Component {

   componentDidMount() {
      // const obj_image = $('.container-offshoreImg img')[0];
      // obj_image.height = obj_image.clientWidth * 2 / 3;

      $(window).on("scroll", function () {
         if (130 < $(this).scrollTop() && $(this).scrollTop() < 500)
            $('.offshoreImg .animated').addClass('fadeInUp');
      });
   }

   render() {
      return (
         <React.Fragment>
            <section className="offshoreImg">
               <div className="container text-center py-3 py-md-4">
                  <p className="font-RoL custom_title" style={{color: '#403e3d'}}>
                     Offshore Development <span className="color_sna">Center</span>
                  </p>
                  <div className="font-RoR custom_content" style={{color: '#777777'}}>
                     With BuildAppFor.me, your goal is already success. While we offer services as many other
                     offshore <br/>
                     company do, SNA is specialized on dedicated team, security and reliability.
                  </div>
               </div>
               <div className="container row py-3 py-md-4 m-auto">
                  <div className="col-12 col-md-4  animated p-3 delay-1">
                     <div className="container-offshoreImg position-relative w-100"
                          onClick={() => this.HandleMovePage('own-team')}>
                        <img src={img} alt="alt" className="w-100"/>
                        <div className="containerBack-offshoreImg position-absolute">
                           <i className="fa fa-plus" aria-hidden="true"/>
                        </div>
                     </div>
                     <p className="custom_title_card mt-3 mt-md-4 mb-1">Dedicated Team</p>
                     <div className="custom_content">
                        A dedicated team is provided for your project and available to scale up and down at your need.
                        We understand communication is the key to the success. All our team members are fluent in verbal
                        and written English.
                     </div>
                  </div>
                  <div className="col-12 col-md-4 animated  p-3 delay-2">
                     <div className="container-offshoreImg position-relative w-100"
                          onClick={() => this.HandleMovePage('security')}>
                        <img src={img1} alt="alt" className="w-100"/>
                        <div className="containerBack-offshoreImg position-absolute">
                           <i className="fa fa-plus" aria-hidden="true"/>
                        </div>
                     </div>
                     <p className="custom_title_card mt-3 mt-md-4 mb-1">Security </p>
                     <div className="custom_content">
                        With the Security-First policy, we consider your data and information very seriously. All
                        project members are required to sign NDA before any project starts. We are well prepared on both
                        logical and physical security to protect your intellectual property.
                     </div>
                  </div>
                  <div className="col-12 col-md-4 animated  p-3 delay-3">
                     <div className="container-offshoreImg position-relative w-100"
                          onClick={() => this.HandleMovePage('reliability')}>
                        <img src={img2} alt="alt" className="w-100"/>
                        <div className="containerBack-offshoreImg position-absolute">
                           <i className="fa fa-plus" aria-hidden="true"/>
                        </div>
                     </div>
                     <p className="custom_title_card mt-3 mt-md-4 mb-1">Reliability</p>
                     <div className="custom_content">
                        Each our managers are experienced +10 years and embodied with the software development process.
                        Experienced in numerous areas including mobile, finance, retails, and smart factory, we suggest
                        best way to achieve your goal.
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }

   HandleMovePage = (path) => {
      $('.nav-item').removeClass('active');
      $('.why-us_1').addClass('active');
      this.props.history.push('why-us#' + path);

   }
}

export default withRouter(Offshore);

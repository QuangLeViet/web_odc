export const ListContactUs = [
   {
      title:'ready to realize your idea?',
      content:'Tell us your expectations and we will find the way to make it happen.'
   },
   {
      title:'Looking for quote?',
      content:'ContactUs us to find out'
   },
   {
      title:'Ready to realize your idea?',
      content:'ContactUs us for more details'
   },
   {
      title:'Looking for more information?',
      content:'Give us a chance to introduce ourselves'
   },
   {
      title:'Need more details?',
      content:'Let us know what you need to know'
   },
   {
      title:'Looking for contact?',
      content:'Leave us your contact information, we will get back to you in a moment'
   },
   {
      title:'Not convinced yet?',
      content:'Find out more what we can offer'
   },
];

export const getRandomInt= (max) =>(Math.floor(Math.random() * Math.floor(max)));

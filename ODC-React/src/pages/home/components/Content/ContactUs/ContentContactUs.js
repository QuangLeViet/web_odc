import React, {Component} from 'react';
import OurVision from "./OurVision/OurVision";
import OurJourney from "./OurJourney/OurJourney";
import DiscussProject from "./DiscussProject/DiscussProject";
import $ from "jquery";
import {withRouter} from 'react-router-dom';

class ContentContactUs extends Component {

   componentDidMount() {
      const tag_ele = this.props.location.hash;
      if (tag_ele) {
         setTimeout(function () {
            $('html, body').animate({
               scrollTop: $(tag_ele).offset().top - 100
            }, 500);
         }, 2000);
      }
   }

   render() {
      return (
         <React.Fragment>

            <OurVision/>

            <OurJourney/>

            <DiscussProject/>

         </React.Fragment>
      )
   }
}

export default withRouter(ContentContactUs);
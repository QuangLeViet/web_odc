import React, {Component} from 'react';
import './index.css';

class OurJourney extends Component {


   render() {
      return (
         <React.Fragment>
            <section className="journey my-3 my-md-5 py-3 py-md-5">
               <div className="container">
                  <p className="custom_title mt-3">
                     <span className="color_sna">OUR</span> HISTORY
                  </p>

                  <div className="wrapper">
                     <div className="timeline">
                        <dl className="timeline--entry">
                           <dt className="timeline--entry__title px-3 pt-4 pb-2 font-RoR">2016 - Company Foundation</dt>
                           <dd className="timeline--entry__detail px-3 font-RoR">We founded company in assotiation from
                              SNA Korea. <div style={{opacity: 0}}>We founded company in assotiation from
                                 SNA Korea.We founded company in assotiation from
                                 SNA Korea.</div>

                           </dd>
                        </dl>
                        <dl className="timeline--entry">
                           <dt className="timeline--entry__title px-3 pt-4 pb-2 font-RoR">2017 - ODC Business</dt>
                           <dd className="timeline--entry__detail px-3 font-RoR"> Our Development journey started with 8
                              customers by end of the year.<div style={{opacity: 0}}>We founded company in assotiation from
                                 SNA Korea.We founded company in assotiation from
                                 SNA Korea.</div>
                           </dd>
                        </dl>
                        <dl className="timeline--entry">
                           <dt className="timeline--entry__title px-3 pt-4 pb-2 font-RoR">2018 - Company Expansion</dt>
                           <dd className="timeline--entry__detail px-3 font-RoR">Expanding our offshrore development
                              business to 316%<div style={{opacity: 0}}>We founded company in assotiation from
                                 SNA Korea.We founded company in assotiation from
                                 SNA Korea.</div>
                           </dd>
                        </dl>
                        <dl className="timeline--entry">
                           <dt className="timeline--entry__title px-3 pt-4 pb-2 font-RoR">2019 - Expansion to US</dt>
                           <dd className="timeline--entry__detail px-3 font-RoR">We opened branch office in San Jose, US
                              keep expending our business world-wide.<div style={{opacity: 0}}>We founded company in assotiation from
                                 SNA Korea.We founded company in assotiation from
                                 SNA Korea.</div>
                           </dd>
                        </dl>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default OurJourney;
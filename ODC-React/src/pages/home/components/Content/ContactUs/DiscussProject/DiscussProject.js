import React, {Component} from 'react';
import './index.css';
import {Form, Input, Button, Checkbox} from 'antd';
import callApi from "../../../../../../axios";
import Swal from 'sweetalert2'
import Loading from "../../../Loading/Loading";

const {TextArea} = Input;

class DiscussProject extends Component {

   state = {
      loading: false,
      yourProject: '',
      aboutSNA: ''
   };

   render() {

      const {getFieldDecorator} = this.props.form;
      return (
         <React.Fragment>
            <Loading loading={this.state.loading}/>
            <section className="discuss position-relative">
               <div className="container-discuss" id="send-info">
                  <p className="container-discuss_title font-weight-bold font-RoR">
                     Want to discuss your project ?
                  </p>
                  <p className="container-discuss_noteTitle font-RoR">
                     Let's talk! We'd love to hear about your idea.
                  </p>
                  <Form onSubmit={this.handleSubmit}>
                     <div className="d-flex flex-wrap no-gutters">
                        {/*INPUT*/}
                        <div className="col-12 col-md-6 pr-md-2">
                           <Form.Item>
                              {getFieldDecorator('fullName', {
                                 rules: [{required: true, message: 'Please input your Full Name!'}],
                              })(
                                 <Input placeholder="Full Name"/>,
                              )}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-6 pl-md-2">
                           <Form.Item>
                              {getFieldDecorator('company', {
                                 rules: [{required: true, message: 'Please input your Company!'}],
                              })(
                                 <Input placeholder="Company"/>,
                              )}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-6 pr-md-2">
                           <Form.Item>
                              {getFieldDecorator('email', {
                                 rules: [{required: true, message: 'Please input your Email!'}],
                              })(
                                 <Input placeholder="Email"/>,
                              )}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-6 pl-md-2">
                           <Form.Item>
                              {getFieldDecorator('phone', {
                                 rules: [{required: true, message: 'Please input your Phone!'}],
                              })(
                                 <Input placeholder="Phone"/>,
                              )}
                           </Form.Item>
                        </div>

                        {/*9 SELECT*/}
                        <div className="w-100 my-3 font-weight-bold font-RoR container-discuss_subTitle">HOW CAN WE
                           HELP?
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help1', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Custom Software</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help2', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Product Strategy</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help3', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Research Sprint</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help4', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Mobile Development</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help5', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">UX & Product Design</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help6', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize"> Design Sprint</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help7', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Web Development</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help8', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Product Prototype</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('help9', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Other</Checkbox>)}
                           </Form.Item>
                        </div>

                        {/*TEXTAREA*/}
                        <div className="w-100 my-3 font-weight-bold font-RoR container-discuss_subTitle"> PLEASE BRIEFLY DESCRIBE YOUR PROJECT
                        </div>
                        <div className="col-12">
                           <Form.Item>
                              <TextArea rows="3" allowClear
                                        value={this.state.yourProject}
                                        onChange={this.onChange}
                                        placeholder="Give us some background about your project, the goals, requirement,
                                 anything that will help us get an understanding or your project"/>
                           </Form.Item>
                        </div>

                        {/*4 SELECT*/}
                        <div className="w-100 mt-3 mb-4 font-weight-bold font-RoR container-discuss_subTitle">WHAT IS
                           YOUR
                           BUDGET ?
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('budget1', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">Less than $10,000</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('budget2', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">$10,000 - $25,000</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4"/>

                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('budget3', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">$25,000 - $50,000</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4">
                           <Form.Item>
                              {getFieldDecorator('budget4', {
                                 valuePropName: 'checked',
                                 initialValue: false,
                              })(<Checkbox className="text-capitalize">more than $50,000</Checkbox>)}
                           </Form.Item>
                        </div>
                        <div className="col-12 col-md-4"/>


                        {/*TEXT AREA*/}
                        <div className="w-100 mt-3 mb-4 font-weight-bold font-RoR container-discuss_subTitle">HOW DID
                           YOU HEAR ABOUT SNA?
                        </div>
                        <div className="col-12">
                           <Form.Item>
                              <TextArea rows="3" allowClear
                                        value={this.state.aboutSNA}
                                        onChange={this.onChange2}
                                        placeholder="Referral, search, social media, blog, etc."/>
                           </Form.Item>
                        </div>

                        <Button type="primary" htmlType="submit" className="container-discuss_btn">
                           LET'S GET STARTED
                        </Button>
                     </div>
                  </Form>
               </div>
            </section>
         </React.Fragment>
      )
   }

   onChange = ({target: {value}}) => {
      this.setState({yourProject: value})
   };

   onChange2 = ({target: {value}}) => {
      this.setState({aboutSNA: value})
   };

   handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
         if (!err) {
            const temp = {
               body: `<h1>Customer discuss their project</h1>
            <p>Customer Full Name: <b>${values.fullName}</b></p>
            <p>Customer Email: <b>${values.email}</b></p>
            <p>Customer Phone: <b>${values.phone}</b></p>
            <p>Customer Company: <b>${values.company}</b></p>
            <p>How we can help : 
            <b>${values.help1 ? 'Custom Software, ' : ''}</b><b>${values.help2 ? 'Product Strategy, ' : ''}</b> 
            <b>${values.help3 ? 'Research Sprint, ' : ''}</b><b>${values.help4 ? 'Mobile Development, ' : ''}</b>  
            <b>${values.help5 ? 'UX & Product Design, ' : ''}</b><b>${values.help6 ? 'Design Sprint, ' : ''}</b>
            <b>${values.help7 ? 'Web Developmet, ' : ''}</b><b>${values.help8 ? 'Product Prototype, ' : ''}</b> 
            <b>${values.help9 ? 'Other ' : ''}</b></p>
            <p>Project Description : <b>${this.state.yourProject}</b></p>
             <p>Budget : 
            <b>${values.budget1 ? 'Less Than $10,000, ' : ''}</b><b>${values.budget2 ? '$10,000 - $25,000, ' : ''}</b>
            <b>${values.budget3 ? '$25,000 - $50,000, ' : ''}</b><b>${values.budget4 ? 'More Than $50,000 ' : ''}</b></p>
            <p>Hear About SNA : <b>${this.state.aboutSNA}</b></p>`
            };
            this.setState({loading: true});
            callApi('POST', `send-email`, temp).then(res => {
               this.setState({loading: false});
               Swal.fire(
                  'Success!',
                  'You send success!',
                  'success'
               )
            }, err => {
               Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: err.message
               })
            })
         }
      });
   };
}

const WrappedNormalLoginForm = Form.create({name: 'normal_login'})(DiscussProject);
export default WrappedNormalLoginForm;
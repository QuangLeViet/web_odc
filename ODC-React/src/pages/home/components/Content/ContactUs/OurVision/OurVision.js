import React, {Component} from 'react';
import './index.css';
import imgWorkTable from './../../../../../../assests/images/aboutus/work-table.png';

class OurVision extends Component {

   render() {
      return (
         <React.Fragment>
            <section className="contactUs mb-3 mb-md-5">
               <div className="container">
                  <div className="row">
                     <div className="col-12 col-md-8 my-2 my-md-0">
                        <p className="custom_title mt-3">
                           ABOUT <span className="color_sna">US</span>
                        </p>
                        <div className="custom_content w-85 text-justify my-3 mt-md-4 mb-md-5">
                           BuildAppFor.Me is formed to enthusiasts believe in App can change way of our thinking, our
                           behaviors and eventually way of living. With experiences from many projects and enterprise
                           level managements, we are wannabe of your next great thing. We will keep pushing our limits
                           to success because we believe your success is ours.
                        </div>
                        <img src={imgWorkTable} className="w-100" alt=""/>
                     </div>
                     <div className="col-12 col-md-4 my-2 my-md-0">
                        <div className="w-100 h-100 p-4 bg_color_sna">
                           <p className="custom_title mt-2 text-white">
                              OUR MISSION
                           </p>
                           <div className="custom_content my-3 mt-md-4 mb-md-5 text-justify text-white">
                              We solve our clients' toughest challenges by providing unmatched services in strategy,
                              consulting, digital, technology and operations. We partner with global enterprises and
                              solution providers, driving innovation to improve the way the world works and lives. With
                              expertise various industries and all business functions, we deliver transformational
                              outcomes for a demanding new digital world.
                              <br/>
                              <br/>
                              Our mission is Q.P.C, stands for Quality, Punctuality and Communication.
                              Quality is our most important mission, any tasks that we are confront of. We believe
                              superior quaility comes from details. We consider every little single details during our
                              production.
                              <br/>
                              <br/>
                              Punctuality is our promise we make with our customers and ourselves. Even in the toughest
                              environment, we will do our best to keep everyting in time.
                              <br/>
                              <br/>
                              Last but not least, we will communicate with our customer whenever and whatever it takes.
                              We trust that open communication is essential key for success, therefore we will not
                              hesitate to communicate in advance with our customers.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default OurVision;
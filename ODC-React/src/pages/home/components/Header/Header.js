import React, {Component} from 'react';
import './index.css';
import $ from "jquery";
import {Link, withRouter} from 'react-router-dom';
import {list_content} from "./ContentJSON";
import logo from './../../../../assests/images/header/bafm-white-text.svg';
import logoBlue from './../../../../assests/images/header/bafm-black-logo.png';

class Header extends Component {

   state = {
      toggle: false,
      logo: logo,
      href: ''
   };

   componentDidMount() {
      if (this.state.href !== this.props.location.pathname) {
         this.setState({href: this.props.location.pathname})
      }
      this.HandleScrollPage();
   }

   HandleScrollPage = () => {
      const self = this;
      let positionY = 0;
      $(window).on("scroll", function () {
         if ($(this).scrollTop() > positionY && $(this).scrollTop() > 10) {
            $('.home_header').addClass('nav-up');
            positionY = $(this).scrollTop();
         } else {
            $('.home_header').removeClass('nav-up');
            positionY = $(this).scrollTop();
         }
         // WHEN SCROLL DOWM > 150
         if ($(this).scrollTop() > 150) {
            self.HandleChangeMenuBlue();
            return;
         }
         // WHEN SCROLL UP < 150
         self.HandleChangeMenuWhite();
      });
   };

   HandleChangeMenuBlue = () => {
      if (this.state.logo === logo) {
         $('.home_header').addClass('bg-white');
         $('.nav-link, .custom_languages').removeClass('text-white').addClass('text-dark');
         if ($(window)[0].innerWidth < 768) {
            $('.nav-item.active .nav-link').removeClass('text-dark').addClass('text-white');
         }
         this.setState({logo: logoBlue});
      }
   };

   HandleChangeMenuWhite = () => {
      if (this.state.toggle) {
         $('.home_header .small_screen .button_collapse').addClass('d-none').removeClass('show');
         this.setState({toggle: false})
      }
      if (this.state.logo !== logo) {
         $('.home_header').removeClass('bg-white');
         $('.nav-link, .custom_languages').addClass('text-white').removeClass('text-dark');
         this.setState({logo: logo});
      }
   };
   HandleGoToHome = () => {
      $('.nav-item').removeClass('active');
      $('._1').addClass('active');
   }

   render() {
      return (
         <React.Fragment>
            <header className={"home_header position-fixed w-100 px-3 px-lg-0"}>
               {/*DESKTOP*/}
               <nav className="navbar navbar-expand-lg navbar-light big_screen">
                  <div className={"button_collapse "}>
                     <Link to={'/'} onClick={this.HandleGoToHome}>
                        <img src={this.state.logo} alt="logo" className=""/>
                     </Link>
                     <ul className="navbar-nav mt-2 mt-lg-0 w-100 justify-content-end mr-lg-1">
                        {list_content.map((ele, ind) => (
                           <li key={ele.href}
                               className={(this.state.href === `/${ele.href}` ? "active " : "") + `${ele.href}_1 nav-item pl-md-1`}
                               onClick={(event) => this.HandleClick(event, ele.href)}>
                              <span
                                 className={"font-RoM cursor-pointer text-uppercase nav-link text-white position-relative"}>{ele.name}</span>
                           </li>
                        ))}
                     </ul>
                  </div>
               </nav>

               {/*MOBILE*/}
               <nav className="navbar navbar-expand-lg navbar-light small_screen">
                  <Link to={'/'} onClick={this.HandleGoToHome}>
                     <img src={this.state.logo} alt="logo" className=""/>
                  </Link>
                  <div className={(this.state.toggle ? "open" : "") + " custom_hamburger"}
                       onClick={this.HandleToggle}>
                     <span/>
                     <span/>
                     <span/>
                  </div>
                  <div className={"button_collapse d-none"}>
                     <ul className="navbar-nav mt-4 mt-lg-0 w-100 justify-content-end">
                        {list_content.map((ele, ind) => (
                           <li key={ele.href} className={(this.state.href === ele.href ? "active " : "")
                           + "nav-item font-weight-bold cursor-pointer"}
                               onClick={(event) => this.HandleClick(event, ele.href)}>
                              <span className={"font-RoM nav-link text-white text-uppercase animated"}>{ele.name}</span>
                           </li>
                        ))}
                     </ul>
                  </div>
               </nav>
            </header>
         </React.Fragment>
      )
   }

   HandleToggle = () => {
      const self = this;
      const small_btn_collapse = $('.small_screen .button_collapse');
      this.setState({toggle: !this.state.toggle}, function () {
         // CLOSE
         if (!self.state.toggle) {
            small_btn_collapse.removeClass('show');
            setTimeout(function () {
               if ($(window).scrollTop() < 50) {
                  self.HandleChangeMenuWhite();
               }
               small_btn_collapse.addClass('d-none');
            }, 100);
            return;
         }
         //OPEN
         self.HandleChangeMenuBlue();
         small_btn_collapse.addClass('show opening').removeClass('d-none');
         setTimeout(function () {
            small_btn_collapse.removeClass('opening');
         }, 100);
      });
   };

   HandleClick = (event, ele) => {
      event.persist();
      $('.nav-item').removeClass('active fadeInLeft');
      if (event.target.className.includes('nav-item')) {
         event.target.className += ' active fadeInLeft';
      } else {
         event.target.parentElement.className += ' active fadeInLeft';
      }

      this.HandleToggle();
      $(window).scrollTop(0);
      this.props.history.push(`/${ele}`);
   }

}

export default withRouter(Header);

import React, {Component} from 'react';
import './index.css';
import logo from './../../../../assests/images/header/bafm-black-text.svg';
import $ from 'jquery';
import callApi from "../../../../axios";
import Swal from "sweetalert2";
import Loading from "../Loading/Loading";
import {Link} from "react-router-dom";

class Footer extends Component {
   state = {
      show: false,
      name: '',
      email: '',
      message: '',
      loading: false,
   };

   componentDidMount() {
   }

   render() {
      return (
         <React.Fragment>
            <Loading loading={this.state.loading}/>
            <footer className="container m-auto py-3 pt-md-5 pb-md-4">
               <div className="footer-map row m-auto">
                  <div className="col-12 col-md-4 p-3 p-md-1 p-lg-3 animated delay-1">
                     <div className="container-map text-center">
                        <div className="map px-3 pt-4">
                           <iframe title="hanoi"
                                   src="https://www.google.com/maps?q=21.173940,106.065268&z=16&output=embed"
                                   frameBorder="0" style={{border: 0}} allowFullScreen=""/>
                        </div>
                        <p className="title-map font-Utm py-4 m-0">
                           Hanoi Office
                        </p>
                     </div>
                     <div className="content-map text-center pt-4">
                        {/*<p className="phoneNumber font-RoM">+1234567890</p>*/}
                        <div className="triangle-down">
                           <div className="triangle-down-left"/>
                           <div className="triangle-down-right"/>
                           <p className="address font-RoR">9th Floor VNPT Bac Ninh Building, 33 Ly Thai To St Ninh Xa
                              Ward, Bac
                              Ninh Province</p>
                        </div>
                     </div>
                  </div>
                  <div className="col-12 col-md-4 p-3 p-md-1 p-lg-3 animated">
                     <div className="container-map text-center">
                        <div className="map px-3 pt-4">
                           <iframe title="korea" frameBorder="0" style={{border: 0}} allowFullScreen=""
                                   src="https://www.google.com/maps?q=37.549657,-122.316532&z=16&output=embed"/>
                        </div>
                        <p className="title-map font-Utm py-4 m-0">
                           U.S Office
                        </p>
                     </div>
                     <div className="content-map text-center pt-4">
                        {/*<p className="phoneNumber font-RoM">+1234567890</p>*/}
                        <div className="triangle-down">
                           <div className="triangle-down-left"/>
                           <div className="triangle-down-right"/>
                           <p className="address font-RoR">155 Bovet Rd Suite 465, San Mateo, CA 94402</p>
                        </div>
                     </div>
                  </div>
                  <div className="col-12 col-md-4 p-3 p-md-1 p-lg-3 animated delay-2">
                     <div className="container-map text-center">
                        <div className="map px-3 pt-4">
                           <iframe title="hcm"
                                   src="https://www.google.com/maps?q=10.755720,106.690947&z=16&output=embed"
                                   frameBorder="0" style={{border: 0}} allowFullScreen=""/>
                        </div>
                        <p className="title-map font-Utm py-4 m-0">
                           Saigon Office
                        </p>
                     </div>
                     <div className="content-map text-center pt-4">
                        {/*<p className="phoneNumber font-RoM">+1234567890</p>*/}
                        <div className="triangle-down">
                           <div className="triangle-down-left"/>
                           <div className="triangle-down-right"/>
                           <p className="address font-RoR">V5 Office, 6th floor, KH2 Apartment, 360A Ben Van Don St,
                              Ward 1,
                              District 4</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div className="footer-bottom d-flex justify-content-between align-items-end pb-1">
                  <img src={logo} alt="sna"/>
                  <div className="group-icons">
                     <Link to={'/about-us#send-info'}><i className="fa fa-envelope text-dark"/></Link>
                     <a href={'https://www.facebook.com/snaglobal.net/'} target="_blank">  <i className="fa fa-facebook px-3 text-dark"/></a>
                     <Link to={'/'}><i className="fa fa-globe text-dark"/></Link>
                  </div>
               </div>
               <div className="text-center pt-2">
                  <span className="copyright pr-1"> <i
                     className="fa fa-copyright"/> 2019 Sna. All rights reserved </span>
                  <a href="https://app.termly.io/document/privacy-policy/2f200039-75a1-4e19-863c-681f2158fd2b"
                     target="_blank" className="color_sna pl-2"> Privacy</a>
               </div>
            </footer>
            <div className="position-fixed bottom-0 right-0" style={{zIndex: 1}}>
               <div className={
                  " position-absolute position_mes bottom-0 right-0"}>
                  <div className="position-relative">
                     {/*button */}
                     <div className={" btn_mes fa animated bounceIn delay-1"}
                          onClick={this.handleShowMes}/>

                     {/*offline*/}
                     <div className="custom_popover position-absolute left">
                        <div className="arrow"/>
                        <div className="custom_popover-header">
                           <p className="custom_popover-title font-RoR m-0">Send us a message</p></div>
                        <div className="custom_popover-content font-RoR">We will get right back to you.</div>
                     </div>


                     {/*boxes*/}
                     <div className="content-mes">
                        <div className="header-mes text-white font-RoM d-flex
                            justify-content-center align-items-center">
                           <div className="opacity-btn_mes mr-3"/>
                           Please fill out the form!
                        </div>
                        <div className="body-mes">
                           <input type="text" className="font-RoM" name="name"
                                  value={this.state.name} onChange={this.handleChange}
                                  placeholder="Enter your name"/>
                           <input type="text" className="font-RoM" name="email"
                                  value={this.state.email} onChange={this.handleChange}
                                  placeholder="Enter your email address ( Required )"/>
                           <textarea rows="8" className="font-RoM" name="message"
                                     value={this.state.message} onChange={this.handleChange}
                                     placeholder="Enter your message and hit Enter"/>
                           <button className="float-right text-white bg_color_sna font-RoM"
                                   onClick={this.HandleSendEmail}>SUBMIT
                           </button>
                        </div>
                     </div>

                  </div>
               </div>
            </div>

         </React.Fragment>
      )
   }

   handleChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      this.setState({
         [name]: value
      });
   }

   handleShowMes = () => {
      const show = this.state.show;
      if (!show) {
         $('.position_mes , .btn_mes').addClass('active fadeInUpMes').removeClass('fadeOutMes');
      } else {
         $('.position_mes , .btn_mes').removeClass('fadeInUpMes').addClass('fadeOutMes');
         setTimeout(function () {
            $('.position_mes , .btn_mes').removeClass('active')
         }, 300)
      }
      this.setState({show: !show});
   };

   HandleSendEmail = (event) => {
      event.preventDefault();
      const temp = {
         body: `<h1>Customer Inquery <small> By Chat </small></h1>
            <p>Customer Name: <b>${this.state.name}</b></p>
            <p>Customer Email:<b>${this.state.email}</b></p>
            <p>Message:<b>${this.state.message}</b></p>`
      };
      this.setState({loading: true});
      callApi('POST', `send-email`, temp).then(res => {
         this.setState({loading: false});
         Swal.fire(
            'Success!',
            'You send success!',
            'success'
         )
         this.handleShowMes();
      })
   }
}

export default Footer;

import React, {Component} from 'react';
import './index.css';

class Loading extends Component {

   render() {
      return (
         <React.Fragment>
            {this.props.loading ?
               (<div className="container-cube-wrapper">
                  <div className="circle"/>
                  <div className="circle-small"/>
                  <div className="circle-big"/>
               </div>) :
               null
            }

            {/*<div className="container-cube-wrapper">*/}
            {/*   <div className="cube-wrapper">*/}
            {/*      <div className="cube-folding">*/}
            {/*         <span className="leaf1"/>*/}
            {/*         <span className="leaf2"/>*/}
            {/*         <span className="leaf3"/>*/}
            {/*         <span className="leaf4"/>*/}
            {/*      </div>*/}
            {/*      <span className="loading" data-name="Loading">Loading</span>*/}
            {/*   </div>*/}
            {/*</div>*/}
         </React.Fragment>
      )
   }
}




export default (Loading);

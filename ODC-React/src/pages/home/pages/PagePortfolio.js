import React, {Component} from 'react';
import Loading from "../components/Loading/Loading";
import ContentPortfolio from "../components/Content/Portfolio/ContentPortfolio";
import ContactUs from "../components/Content/Home/ContactUs/ContactUs";

class PagePortfolio extends Component {

   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 2000)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentPortfolio/>

            {/*Content*/}
            <ContactUs/>
         </React.Fragment>
      )
   }
}

export default (PagePortfolio);

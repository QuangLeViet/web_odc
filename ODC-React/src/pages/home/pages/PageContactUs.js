import React, {Component} from 'react';
import ContentContactUs from "../components/Content/ContactUs/ContentContactUs";
import Loading from "../components/Loading/Loading";

class PageContactUs extends Component {

   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 2500)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentContactUs/>
         </React.Fragment>
      )
   }
}

export default PageContactUs;
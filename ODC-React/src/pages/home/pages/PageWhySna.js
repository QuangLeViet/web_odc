import React, {Component} from 'react';
import ContentWhySna from "../components/Content/WhySna/ContentWhySna";
import Loading from "../components/Loading/Loading";
import ContactUs from "../components/Content/Home/ContactUs/ContactUs";

class PageWhySna extends Component {

   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 2000)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentWhySna/>

            {/*Content*/}
            <ContactUs/>
         </React.Fragment>
      )
   }
}

export default (PageWhySna);

import React, {Component} from 'react';
import Loading from "../components/Loading/Loading";
import ContentDetailPortfolio from "../components/Content/DetailPortfolio/ContentDetailPortfolio";
import ContactUs from "../components/Content/Home/ContactUs/ContactUs";

class PageDetailPortfolio extends Component {

   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 2500)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentDetailPortfolio/>

            {/*Content*/}
            <ContactUs/>

         </React.Fragment>
      )
   }
}

export default (PageDetailPortfolio);

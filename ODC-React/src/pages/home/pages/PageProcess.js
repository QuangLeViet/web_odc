import React, {Component} from 'react';
import Loading from "../components/Loading/Loading";
import ContentProcess from "../components/Content/Process/ContentProcess";
import ContactUs from "../components/Content/Home/ContactUs/ContactUs";

class PageProcess extends Component {
   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 2000)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentProcess/>

            {/*Content*/}
            <ContactUs/>
         </React.Fragment>
      )
   }
}

export default (PageProcess);

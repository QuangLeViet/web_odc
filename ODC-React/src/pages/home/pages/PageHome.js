import React, {Component} from 'react';
import ContentHome from "../components/Content/Home/ContentHome";
import Loading from "../components/Loading/Loading";
import ContactUs from "../components/Content/Home/ContactUs/ContactUs";

class PageHome extends Component {

   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 2000)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentHome/>

            {/*Content*/}
            <ContactUs/>
         </React.Fragment>
      )
   }
}

export default (PageHome);

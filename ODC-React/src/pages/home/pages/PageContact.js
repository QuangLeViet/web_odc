import React, {Component} from 'react';
import Header from "../components/Header/Header";

import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import SectionMap from "../components/Content/ContactUs/SectionMap";

class PageContact extends Component {

   componentDidMount() {
      let chart = am4core.create("chartdiv", am4maps.MapChart);

      chart.geodata = am4geodata_worldLow;

// Set projection
      chart.projection = new am4maps.projections.Miller();

// Create map polygon series
      var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Make map load polygon (like country names) data from GeoJSON
      polygonSeries.useGeodata = true;

// Remove Antarctica
      polygonSeries.exclude = ["AQ"];      // ... chart code goes here ...

      this.chart = chart;
   }

   UNSAFE_componentWillMount() {
      if (this.chart) {
         this.chart.dispose();
      }
      // return localStorage.getItem('auth') === ('' || null) ? this.props.history.push("/login") : null;
   }

   render() {
      return (
         <React.Fragment>
            {/*Header*/}
            <Header/>

            {/* Content 1 */}
            <section id="content2">
               <div className="text-center py-3 py-md-5">
                  <span className="custom_title">Offshore Development Center</span>
                  <SectionMap/>
               </div>
            </section>


            {/*Footer*/}
            {/*<Footer/>*/}
         </React.Fragment>
      )
   }
}

export default (PageContact);
import React, {Component} from 'react';
import {Switch, Route} from "react-router-dom";
import {list_router_pages} from "../../router";
import Header from "./components/Header/Header";
import ContactUs from "./components/Content/Home/ContactUs/ContactUs";
import Footer from "./components/Footer/Footer";

class MainLayout extends Component {
   render() {
      return (
         <React.Fragment>
            {/*Header*/}
            <Header/>

            {/*Content*/}
            <Switch>
               {this.GetRouter(list_router_pages)}
            </Switch>

            {/*Footer*/}
            <Footer/>

         </React.Fragment>
      )
   }

   GetRouter = (routers) => (
      routers.map((routes) => (
         (routes.routes && (routes.path === '/')) ?
            routes.routes.map((router, i) => (
               <Route key={i} path={`${routes.path + router.path}`} component={router.main} exact={router.exact}/>
            )) : null
      ))
   )
}

export default (MainLayout);

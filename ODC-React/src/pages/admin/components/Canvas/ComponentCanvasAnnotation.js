import React, {Component} from 'react';
import './StylesCanvas.css';
import * as Annotation from './ScriptCanvas';

class ComponentCanvasAnnotation extends Component {

   state = {
      clickOnCanvas: false,
      annotation_type: 'polygon' // dots, rectangle, polygon
   }

   componentDidMount() {
      Annotation.get_background_canvas();
      Annotation.init_global_canvas(this.state.annotation_type);
      Annotation.handle_keyDown_canvas();
   }

   render() {
      return (
         <React.Fragment>
            <canvas className="canvas_annotaion" id="canvas_annotaion" width="600" height="400"
                    onMouseDown={this.CanvasOnMouseDown}
                    onMouseMove={this.CanvasOnMouseMove}
                    onMouseUp={this.CanvasOnMouseUp}
                    onMouseOut={this.CanvasOnMouseOut}
            />
            <div className="d-flex">
               <button className="btn btn-primary btn-sm rounded"
                       onClick={() => this.HandleChangeType('rectangle')}>Rec
               </button>
               <button className="btn btn-success btn-sm rounded ml-3"
                       onClick={() => this.HandleChangeType('dot')}>Dot
               </button>
               <button className="btn btn-success btn-sm rounded ml-3"
                       onClick={() => this.HandleChangeType('polygon')}>Polygon
               </button>
            </div>

         </React.Fragment>
      )
   }

   CanvasOnMouseDown = (event) => {
      event.persist();
      this.setState({clickOnCanvas: true});
      Annotation.handleOnMouse('down', this.state.annotation_type, event)
   };

   CanvasOnMouseMove = (event) => {
      if (!this.state.clickOnCanvas) return false;
      event.persist();
      Annotation.handleOnMouse('move', this.state.annotation_type, event)
   };

   CanvasOnMouseUp = (event) => {
      if (!this.state.clickOnCanvas) return false;
      event.persist();
      this.setState({clickOnCanvas: false});
      Annotation.handleOnMouse('up', this.state.annotation_type, event);
   };

   CanvasOnMouseOut = (event) => {
      if (!this.state.clickOnCanvas) return false;
      event.persist();
      this.setState({clickOnCanvas: false});
      Annotation.handleOnMouse('out', this.state.annotation_type, event)
   };

   HandleChangeType = (type) => {
      this.setState({annotation_type: type}, function () {
         Annotation.handleChangeType(type);
      });

   };

}

export default ComponentCanvasAnnotation;
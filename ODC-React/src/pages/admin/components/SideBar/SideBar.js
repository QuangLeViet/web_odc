import React, {Component} from 'react';
import {Menu, Icon} from 'antd';
import './SideBar.css';
import {withRouter} from 'react-router-dom';
import {listPages} from "./list-pages";

const {Item} = Menu;

class SideBar extends Component {

   render() {
      return (
         <div id="sidebar-wrapper" data-simplebar data-simplebar-auto-hide="true">
            <div className="brand-logo">
               <span>
                 <h5 className="logo-text">Annotation Admin</h5>
               </span>
            </div>

            {/*Show more at https://ant.design/components/menu/*/}
            <Menu className="menu_custom" onClick={this.handleChangePage}
                  mode="inline" defaultSelectedKeys={['1']} defaultOpenKeys={['sub1']}>

               {/*Set Menu item sub1 from listPages.js */}
               {listPages.map(page => (
                  <Item key={page.key}> <Icon type={page.icon}/> <span>{page.title}</span> </Item>
               ))}
            </Menu>

         </div>
      );
   }

   handleChangePage = (e) => {
      const link = `/admin/${e.key}`;
      this.props.history.push(link);
   };
}

export default withRouter(SideBar);
import React, {Component} from 'react';

class Footer extends Component {

    render(){
        return (
            <React.Fragment>
                <footer className="footer">
                    <div className="container">
                        <div className="text-center text-dark">
                            Copyright © 2018 Dashtreme Admin
                        </div>
                    </div>
                </footer>
            </React.Fragment>
        )
    }
}

export default Footer;
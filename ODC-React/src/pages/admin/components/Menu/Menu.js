import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Menu extends Component {

   render() {
      return (
         <React.Fragment>
            <header className="topbar-nav">
               <nav className="navbar navbar-expand fixed-top">
                  <ul className="navbar-nav mr-auto align-items-center">
                     <li className="nav-item">
                      <span className="nav-link toggle-menu">
                        <i className="icon-menu menu-icon"/>
                      </span>
                     </li>
                  </ul>
                  <ul className="navbar-nav align-items-center right-nav-link">
                     <li className="nav-item">
                         <span className="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
                           <span className="user-profile">
                             <button className="btn btn-primary btn-social-circle py-1 px-2">a</button>
                           </span>
                         </span>
                        <ul className="dropdown-menu dropdown-menu-right">
                           <li className="dropdown-item">
                              <i className="icon-user mr-2"/> Infomation
                           </li>
                           <li className="dropdown-divider"/>
                           <li className="dropdown-item">
                              <Link to={'/login'}>
                                 <i className="icon-power mr-2"/> Logout
                              </Link>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </nav>
            </header>
         </React.Fragment>
      )
   }
}

export default Menu;
import React, {Component} from 'react';
import {connect} from "react-redux";
import {Spin} from 'antd';
import './StyleComponentUser.css';

class ComponentCounter extends Component {
   render() {
      const {num, loading} = this.props;
      return (
         <React.Fragment>
            <div className="d-flex align-items-center justify-content-center">
               <div className={"text-center py-4 w-25 border"}>
                  {num}
               </div>
               <div className={"ml-4"}>
                  <Spin spinning={loading}/>
               </div>
            </div>
         </React.Fragment>
      )
   }
}

const mapStateToProps = state => ({
   loading: state.changeNumber.loading,
   num: state.changeNumber.num
});

export default connect(mapStateToProps, null)(ComponentCounter);
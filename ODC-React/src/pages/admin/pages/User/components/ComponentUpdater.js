import React, {Component} from 'react';
import {connect} from "react-redux";
import {decrementAction, incrementAction} from '../Action'

class ComponentUpdater extends Component {

   render() {
      return (
         <React.Fragment>
            <div className={"d-flex align-items-center h-100"}>
               <div className={"col-2 text-center"}>
                  <button className={"btn btn-danger rounded"} onClick={() => this.HandleClick('decrement')}>-</button>
               </div>
               <div className={"col-2 text-center"}>
                  <button className={"btn btn-primary rounded"} onClick={() => this.HandleClick('increment')}>+</button>
               </div>
            </div>
         </React.Fragment>
      )
   }

   HandleClick = (type) => {
      type.includes('de') ?
         this.props.dispatch(decrementAction()) :
         this.props.dispatch(incrementAction())
   }
}

export default connect()(ComponentUpdater);
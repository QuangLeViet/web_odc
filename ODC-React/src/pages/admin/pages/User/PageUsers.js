import React, {Component} from 'react';
import ComponentCounter from "./components/ComponentCounter";
import ComponentUpdater from "./components/ComponentUpdater";
import callApi from '../../../../axios';

class PageUsers extends Component {

   state = {
      id: 0,
      persons: []
   };

   componentDidMount() {

      // callApi('get', 'persons').then(res => {
      //    const persons = res.data;
      //    console.log(res)
      //    this.setState({persons});
      // })
   }

   render() {
      return (
         <React.Fragment>
            <div className="col-12 col-lg-12">
               <div className="card">
                  <div className="card-header"> PAGE USERS</div>
                  <div className="p-3" style={{height: 500}}>
                     <div className={"row no-gutters"}>
                        <div className="col-6">
                           <ComponentCounter/>
                        </div>
                        <div className="col-6">
                           <ComponentUpdater/>
                        </div>
                     </div>
                     <div className={"row no-gutters mt-5"}>
                        <div className={"col-6"}>
                           <ul>
                              {this.state.persons.map((person) => <li key={person.id}>{person.name}</li>)}
                           </ul>
                        </div>
                        <div className={"col-6"}>
                           <form>
                              <label>
                                 Person ID:
                                 <input className={"text-dark"} type="text" onChange={this.handleChange}/>
                              </label>
                              <button type="button" onClick={this.handleSubmit}>Delete</button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </React.Fragment>
      )
   }

   handleChange = (e) => {
      this.setState({id: e.target.value})
   };

   handleSubmit = () => {

      callApi('get', `persons/${this.state.id}`).then(res => {
         console.log(res);
      }, error => {
         console.log(error);
      })
   }
}

export default PageUsers;
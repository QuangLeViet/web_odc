import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Form, Icon, Input} from 'antd';
import './index.css';

class PageLogin extends Component {
   constructor(props) {
      super(props);
      localStorage.removeItem('auth')
   }

   render() {
      const {getFieldDecorator} = this.props.form;
      return (
         <div className="content_login">
            <Form onSubmit={this.handleSubmit}
                  className="text-center position-absolute rounded p-5 login-form">

               {/*<span className="custom_title">Login</span>*/}

               <Form.Item>
                  {getFieldDecorator('username', {
                     rules: [{required: true, message: 'Please input your username!'}],
                  })(
                     <Input
                        className="mt-4"
                        prefix={<Icon type="user"/>}
                        placeholder="Username"
                     />,
                  )}
               </Form.Item>
               <Form.Item>
                  {getFieldDecorator('password', {
                     rules: [{required: true, message: 'Please input your Password!'}],
                  })(
                     <Input
                        prefix={<Icon type="lock"/>}
                        type="password"
                        placeholder="Password"
                     />,
                  )}
               </Form.Item>
               <button className="btn1 draw-border">
                  {/*<Icon type="poweroff" className=""/>*/}
                  Log In
               </button>
            </Form>
         </div>
      );
   }

   handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
         if (!err) {
            localStorage.setItem('auth', 'success!!!');
            this.props.history.push("/admin")
         }
      });
   };
}

const WrappedNormalLoginForm = Form.create({name: 'normal_login'})(PageLogin);

export default withRouter(WrappedNormalLoginForm);
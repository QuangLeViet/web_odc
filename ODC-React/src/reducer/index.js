import {combineReducers} from "redux";
import {changeNumber} from '../pages/admin/pages/User/Reducer';

export const appReducers = combineReducers({
   changeNumber,
})
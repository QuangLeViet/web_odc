import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {list_router_pages} from './router';
import './assests/css/styles.css';
import './assests/js/scripts';

class App extends Component {

  render() {
    return (
      <React.Fragment>
        <Router>
          <Switch>
            {this.GetRouter(list_router_pages)}
          </Switch>
        </Router>
      </React.Fragment>
    );
  }

  GetRouter = (routers) => (
    routers.map((router, index) => (
      <Route key={index} path={`${router.path}`} component={router.main}
             exact={router.exact}/>
    ))
  )
}

export default (App);

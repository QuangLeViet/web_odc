import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './assests/css/styles.css';
import './assests/js/scripts';
import {applyMiddleware, createStore} from "redux";
import {appReducers} from './reducer'
import createSagaMiddleware from 'redux-saga';
import {watcherSaga} from './saga/sagas';
import {Provider} from "react-redux";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(appReducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watcherSaga);

ReactDOM.render(
   <Provider store={store}>
      <App/>
   </Provider>
   , document.getElementById('wrapper')
);

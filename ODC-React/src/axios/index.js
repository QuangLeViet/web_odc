import axios from 'axios';

export const BASE_URL = '/api/';

export default function callApi(method, endpoint,  body) {
   return axios({
      method: (method === undefined ? 'GET' : method),
      url: `${BASE_URL}${endpoint}`,
      data: body,
   })
}

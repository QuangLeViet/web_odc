const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

// fix bug CORS
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

// API
var routes = require('./routers/routers'); //importing route
routes(app);

// connect Front End ( File BUILD )
app.use(express.static('./src'));
// re-connect when refresh page
app.get('/*', function (req, res) {
   res.sendFile(path.join(__dirname+'/src/index.html'))
});

//  it will run if dont have page
app.use(function (req, res) {
   res.status(404).send({ url: req.originalUrl + ' is not found' });
});

// port
app.listen(8000);

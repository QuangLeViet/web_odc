const mysql = require("mysql");
// const nodeMailer = require('nodemailer');

var API_KEY = '6d0a443075190d156e77b238446e82b0-713d4f73-840861cc';
var DOMAIN = 'buildappfor.me';
const mailgun = require('mailgun-js')({ apiKey: API_KEY, domain: DOMAIN });
var email_sender = "noreply@buildappfor.me";
var email_receipient = ["sales@snaglobal.net"]


let connection = mysql.createConnection({
   host: "10.2.17.38",
   user: "root",
   password: "123456",
   database: "odcdb"
});

connection.connect(function (error) {
   if (error) {
      console.log('error');
   } else {
      console.log('Connected');
   }
})
;

// GET LIST Portfolio and image
const Portfolio_GetList = (req, res) => {
   connection.query("SELECT *, output FROM odc_project", (error, results) => {
      if (error) {
         console.log(error);
      } else {
         res.status(200).send(JSON.stringify(results));
      }
   })
};


// GET FIRST IMAGE OF PORTFOLIO
const Portfolio_GetFirstImage = (req, res) => {
   connection.query(`SELECT * FROM odc_project_detail WHERE iddata = (
   SELECT TRIM(SUBSTRING_INDEX(output, ",", 1)) AS imgId FROM odc_project WHERE iddata=
 ${req.params.id}) LIMIT 0,1`, (error, results) => {

      if (error) {
         console.log(error);
      } else {
         res.status(200).send(JSON.stringify(results));
      }
   });
};

const Send_Email = (req, res) => {

   let mailOptions = {
      from: email_sender, // sender address
      to: email_receipient, // list of receivers
      subject: "Customer Inquiry", // Subject line
      html: req.body.body //, // plain text body
   };

   //     "body" : "<h1>Customer Inquery <small> By Chat </small></h1><p>Customer Name:
   //     <b>Dango</b>    </p><p>Customer Email:
   //     <b>dangle@snaglobal.net</b>   </p><p>Message:
   //     <b>Hello World!</b>     </p>"
   // }

   mailgun.messages().send(mailOptions, (error, body) => {
      if (error) {
         console.log(">>>>>>>>>>>", error);
         console.log(error);
      }
      //Do what you need to;
      console.log(">>>>>>>>>>>", body);
      res.status(200).send(JSON.stringify(body));
   });
};

// GET A PORTFOLIO
const Portfolio_GetAPortfolio = (req, res) => {
   connection.query(
      `SELECT * FROM odc_project po WHERE ( po.iddata = ${req.params.id} )`,
      (error, results) => {
         if (error) {
            console.log(error);
         } else {
            res.status(200).send(JSON.stringify(results));
         }
      }
   );
};

// GET  IMAGE IN  PORTFOLIO DETAIL
const Portfolio_GetImageDetail = (req, res) => {
   connection.query(
      `SELECT * FROM odc_project_detail pod WHERE ( pod.iddata = ${req.params.id} )`,
      (error, results) => {
         if (error) {
            console.log(error);
         } else {
            res.status(200).send(JSON.stringify(results));
         }
      }
   );
};

// ADD PORTFOLIO
const Portfolio_Add = (req, res) => {
   const sql = `INSERT INTO portfolio(id, title,url) VALUES (default, '${req.body.title}', '${req.body.url}') `;
   connection.query(sql, (error, results) => {
      if (error) {
         res.send({message: error, status_code: 400});
      } else {
         res.send({message: "Add success", status_code: 200});
      }
   });
};

module.exports = {
   Portfolio_GetList,
   Portfolio_GetFirstImage,
   Portfolio_GetAPortfolio,
   Portfolio_GetImageDetail,
   Send_Email
};

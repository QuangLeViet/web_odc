const autobase = {
   // database: {
   //   creteaDB: "/createDB/:database",
   //   createTablePortfolio: "/createTablePortfolio",
   //   createTableDetailPortfolio: "/createTableDetailPortfolio"
   // },
   api: {
      //PORTFOLIO
      portfolio_list: "/api/list-portfolio",
      portfolio_detail: "/api/portfolio/:id",
      portfolio_firstImage: "/api/portfolio-first-image/:id",
      portfolio_getImageDetail: "/api/portfolio-image-detail/:id",
      send_email: "/api/send-email"
   }
};

module.exports = {autobase};

// get all link
const globals = require("./globals");

module.exports = function (app) {
   // get all function query
   var autobase = require("./autobaseControllers");
   // PORTFOLIO

   // // GET LIST PORTFOLIO
   app.route(globals.autobase.api.portfolio_list).get(autobase.Portfolio_GetList);

   // GET  IMAGE IN  PORTFOLIO DETAIL
   app.route(globals.autobase.api.portfolio_detail).get(autobase.Portfolio_GetAPortfolio);

   // // GET A PORTFOLIO FIRST IMAGE
   app.route(globals.autobase.api.portfolio_firstImage).get(autobase.Portfolio_GetFirstImage);


   // GET  IMAGE IN A PORTFOLIO DETAIL
   app.route(globals.autobase.api.portfolio_getImageDetail).get(autobase.Portfolio_GetImageDetail);


   // GET  IMAGE IN A PORTFOLIO DETAIL
   app.route(globals.autobase.api.send_email).post(autobase.Send_Email);

   // // ADD PORTFOLIO
   // app.route(globals.autobase.api.portfolio_add).post(autobase.Portfolio_Add);
};
